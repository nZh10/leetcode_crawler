import argparse
import sys


def tuple_into_str(tags):
    result_tag = ""
    for index in range(len(tags)):
        result_tag = result_tag + tags[index][0] + ','
    return result_tag


def arg_parser(argv):
    parser = argparse.ArgumentParser()
    parser.add_argument("directory", nargs="?", default="notes",
                        help="Specify the output directory.\nIf not, output will be stored in directory 'notes'.")
    parser.add_argument("-d", "--difficulty", nargs="+", dest="lv", choices=["Easy", "Medium", "Hard"],
                        help="Specify the difficulty.\nIf not, problems of all levels will be downloaded.")
    parser.add_argument("-t", "--tags", nargs="+",
                        help="Specify the tag(s) of the problems.")
    parser.add_argument('-v', '--verbose', action="store_true", default=False,
                        help="Verbose output.")
    parser.add_argument('-s', '--status', nargs='+', dest="stat", choices=['ac', 'notac', 'none'],
                        help="Limit the status of problems.\nIf not specified, all problems will be grasped.")
    parser.add_argument('-c', '--code', nargs='?',
                        help="Specify the path of code directory.")
    parser.add_argument('-u', '--username', nargs='?', dest="name",
                        help="Specify the logging name of leetcode.com")
    parser.add_argument('-p', '--password', nargs='?', dest="passwd",
                        help="Specify the logging password of leetcode.com")

    if len(argv) > 1:
        return vars(parser.parse_args())
    else:
        parser.print_help()
        sys.exit(1)
